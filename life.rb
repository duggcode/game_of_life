class GameOfLife
  def initialize(width, height, state)
    @width = width
    @height = height
    @grid = Grid.new @width, @height, state
    @grid.print_grid
    loop do
      evolution
      sleep 1
    end
  end

  def evolution
    @grid.evolve
    @grid.print_grid
  end
end

class Grid
  def initialize(width, height, state)
    @width = width
    @height = height
    @size = width * height
    @cells = Array.new(@size).map! { Cell.new }
    assign_grid state
  end

  def randomly_seed_grid
    @cells.map { |cell| cell.birth! if rand(0..1).zero? }
  end

  def assign_grid(state)
    return randomly_seed_grid unless state

    @cells.map.with_index { |cell, index| cell.birth! unless state[index].zero? }
  end

  def evolve
    new_cell_state = @cells.map.with_index do |cell, index|
      neighbor_cells_alive = get_neighbor_alive_count(index)
      should_live = false
      # Wikipedia
      # 1. Any live cell with two or three live neighbours survives.
      # 2. Any dead cell with three live neighbours becomes a live cell.
      # 3. All other live cells die in the next generation. Similarly, all other dead cells stay dead.
      if (cell.alive? && [2, 3].include?(neighbor_cells_alive)) ||
         (cell.dead? && neighbor_cells_alive == 3)
        should_live = true
      end
      should_live
    end
    @cells.map!.with_index do |cell, index|
      lives = new_cell_state[index]
      lives ? cell.birth! : cell.kill!
      cell
    end
  end

  def get_neighbor_alive_count(index)
    indices = []
    indices << (index - 1) unless beginning_of_row?(index)
    indices << (index + 1) unless end_of_row?(index)

    unless first_row?(index)
      indices << (above(index - 1)) unless beginning_of_row?(index)
      indices << (above(index + 1)) unless end_of_row?(index)
      indices << (above(index))
    end

    unless last_row?(index)
      indices << (below(index) - 1) unless beginning_of_row?(index)
      indices << (below(index) + 1) unless end_of_row?(index)
      indices << (below(index))
    end

    indices.select { |idx| @cells[idx].alive? }.count
  end

  def print_grid
    @cells.map.with_index do |cell, index|
      print "\n" if index && beginning_of_row?(index)
      print cell.print_status
    end
    print "\n"
  end

  def end_of_row?(index)
    ((index + 1) % @width).zero?
  end

  def beginning_of_row?(index)
    (index % @width).zero?
  end

  def first_row?(index)
    index < @width
  end

  def last_row?(index)
    index >= (@size - @width)
  end

  def above(index)
    index - @width
  end

  def below(index)
    index + @width
  end
end

class Cell
  def initialize(alive: false)
    @alive = alive
  end

  def alive?
    @alive
  end

  def dead?
    !@alive
  end

  def birth!
    @alive = true
  end

  def kill!
    @alive = false
  end

  def red(text)
    "\e[31m#{text}\e[0m"
  end

  def green(text)
    "\e[32m#{text}\e[0m"
  end

  def print_status
    if alive?
      green 'O'
    else
      red 'X'
    end
  end
end

# example_block = [
#   0, 0, 0, 0,
#   0, 1, 1, 0,
#   0, 1, 1, 0,
#   0, 0, 0, 0
# ]

# example_blinker = [
#   0, 0, 0, 0, 0,
#   0, 0, 0, 0, 0,
#   0, 1, 1, 1, 0,
#   0, 0, 0, 0, 0,
#   0, 0, 0, 0, 0
# ]

# GameOfLife.new 4, 4, example_block
# GameOfLife.new 5, 5, example_blinker
GameOfLife.new 24, 24, false
